function trim(str) {
	return str.replace(/^\s+|\s+$/gm, "");
}
function getById(id) {
	return document.getElementById(id);
}
function getNumById(id) {
	var e = getById(id);
	if (e == null) {
		return 0;
	}
	var val = e.value;
	if (val == null) {
		return 0;
	}
	val = trim(val);
	if (val == "") {
		return 0;
	}
	if (isNaN(val)) {
		return 0;
	}
	return parseInt(val);
}
// 各门派冰、火、玄、毒、外功、内功系数列表
var roleProps = {
	"sl" : {
		"b" : 1.0,
		"h" : 1.0,
		"x" : 1.5,
		"d" : 1.0,
		"w" : 0.0575,
		"n" : 0.015
	},
	"mr" : {
		"b" : 1.0,
		"h" : 1.0,
		"x" : 1.5,
		"d" : 1.0,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"ts" : {
		"b" : 1.5,
		"h" : 1.0,
		"x" : 1.0,
		"d" : 1.0,
		"w" : 0.0575,
		"n" : 0.015
	},
	"mj" : {
		"b" : 1.0,
		"h" : 1.5,
		"x" : 1.0,
		"d" : 1.0,
		"w" : 0.0575,
		"n" : 0.015
	},
	"gb" : {
		"b" : 1.0,
		"h" : 1.25,
		"x" : 1.0,
		"d" : 1.3,
		"w" : 0.0575,
		"n" : 0.015
	},
	"xy" : {
		"b" : 1.0,
		"h" : 1.3,
		"x" : 1.0,
		"d" : 1.25,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"wd" : {
		"b" : 1.25,
		"h" : 1.2,
		"x" : 1.1,
		"d" : 1.0,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"em" : {
		"b" : 1.2,
		"h" : 1.0,
		"x" : 1.25,
		"d" : 1.0,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"tl" : {
		"b" : 1.15,
		"h" : 1.15,
		"x" : 1.15,
		"d" : 1.15,
		"w" : 0.04375,
		"n" : 0.04375
	},
	"xx" : {
		"b" : 1.0,
		"h" : 1.0,
		"x" : 1.0,
		"d" : 1.5,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"tm" : {
		"b" : 1.0,
		"h" : 1.0,
		"x" : 1.3,
		"d" : 1.3,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"gg" : {
		"b" : 1.0,
		"h" : 1.2,
		"x" : 1.3,
		"d" : 1.0,
		"w" : 0.003875,
		"n" : 0.07125
	},
	"thd" : {
		"b" : 1.3,
		"h" : 1.0,
		"x" : 1.0,
		"d" : 1.2,
		"w" : 0.07125,
		"n" : 0.003875
	}
};
// 目标列表
var targets = [ {
	"name" : "虚竹 - 琅環福地（困难）",
	"value" : "xuzhu",
	"type" : 0,
	"bk" : 60,
	"hk" : 60,
	"xk" : 60,
	"dk" : 60,
	"wf" : 14190,
	"nf" : 14190,
	"cf" : 0
}, {
	"name" : "飘飘鸭 - 寒玉谷",
	"value" : "piaopiaoya",
	"type" : 0,
	"bk" : 90,
	"hk" : 90,
	"xk" : 90,
	"dk" : 90,
	"wf" : 8184,
	"nf" : 8184,
	"cf" : 0
}, {
	"name" : "1800冰抗峨眉",
	"value" : "bingkangEM",
	"type" : 1,
	"bk" : 1800,
	"hk" : 110,
	"xk" : 110,
	"dk" : 110,
	"wf" : 80000,
	"nf" : 40000,
	"cf" : 3000
}, {
	"name" : "1800火抗峨眉",
	"value" : "huokangEM",
	"type" : 1,
	"bk" : 110,
	"hk" : 1800,
	"xk" : 110,
	"dk" : 110,
	"wf" : 80000,
	"nf" : 40000,
	"cf" : 3000
}, {
	"name" : "1800玄抗峨眉",
	"value" : "xuankangEM",
	"type" : 1,
	"bk" : 110,
	"hk" : 110,
	"xk" : 1800,
	"dk" : 110,
	"wf" : 80000,
	"nf" : 40000,
	"cf" : 3000
}, {
	"name" : "1800毒抗峨眉",
	"value" : "dukangEM",
	"type" : 1,
	"bk" : 110,
	"hk" : 110,
	"xk" : 110,
	"dk" : 1800,
	"wf" : 80000,
	"nf" : 40000,
	"cf" : 3000
}, {
	"name" : "2000杂抗峨眉",
	"value" : "zakangEM",
	"type" : 1,
	"bk" : 500,
	"hk" : 500,
	"xk" : 500,
	"dk" : 500,
	"wf" : 80000,
	"nf" : 40000,
	"cf" : 3000
} ];
// 计算属性伤害
function calcSXDamage(role, type, target, targetType, amount, ignore, limit) {
	var props = roleProps[role];
	var propRatio = props[type];
	var kang = target[targetType];
	var s;
	var x = 0;
	if (ignore + 100 <= kang) {
		x = 100;
		s = "被抗死，无伤害";
	} else if (ignore <= kang) {
		x = Math.min(kang - ignore, 100);
		s = "未减完目标抗性，下限不生效，属性伤害效果为" + (100 - x) + "%";
	} else {
		x = Math.max(kang - ignore, limit * (-1));
		s = "已减完目标抗性，下限生效，属性伤害效果为" + (100 - x) + "%";
	}
	var damageRatio = (100 - x) / 100;
	var damage = amount * propRatio * damageRatio;
	var result = {};
	result.damage = Math.round(damage);
	result.note = s;
	if (target.type == 0) { // 怪物
		result.damage = result.damage * 5;
	}
	return result;
}
// 计算外功、内功伤害
function calcWNDamage(role, type, target, targetType, amount) {
	var props = roleProps[role];
	var attackRatio = props[type];
	var defense = target[targetType];
	var result = Math.round(attackRatio * amount * amount / (amount + defense));
	if (target.type == 0) { // 怪物
		result = result * 5;
	}
	return result;
}
// 计算穿刺伤害
function calcCCDamage(target, targetType, ccAttack) {
	var ccDefense = target[targetType];
	var ccDamage = ccAttack - ccDefense;
	if (ccDamage < 0) {
		return 0;
	}
	if (target.type == 0) { // 怪物
		ccDamage = ccDamage * 5;
	}
	return ccDamage;
}
// 计算伤害
function calcDamage() {
	var role = getSelectedRole();
	var target = getSelectedTarget();
	var b0 = getNumById("b_0");
	var b1 = getNumById("b_1");
	var b2 = getNumById("b_2");
	var h0 = getNumById("h_0");
	var h1 = getNumById("h_1");
	var h2 = getNumById("h_2");
	var x0 = getNumById("x_0");
	var x1 = getNumById("x_1");
	var x2 = getNumById("x_2");
	var d0 = getNumById("d_0");
	var d1 = getNumById("d_1");
	var d2 = getNumById("d_2");
	var w0 = getNumById("w_0");
	var w1 = getNumById("w_1");
	var n0 = getNumById("n_0");
	var n1 = getNumById("n_1");
	var c = getNumById("c");
	// 属性伤害计算
	var bDamage = calcSXDamage(role, "b", target, "bk", b0, b1, b2);
	var hDamage = calcSXDamage(role, "h", target, "hk", h0, h1, h2);
	var xDamage = calcSXDamage(role, "x", target, "xk", x0, x1, x2);
	var dDamage = calcSXDamage(role, "d", target, "dk", d0, d1, d2);
	// 外功、内功伤害计算
	var wDamage = calcWNDamage(role, "w", target, "wf", w0 + w1);
	var nDamage = calcWNDamage(role, "n", target, "nf", n0 + n1);
	// 穿刺伤害计算
	var cDamage = calcCCDamage(target, "cf", c);
	// 显示结果
	getById("r_b").innerText = bDamage.damage;
	getById("r_h").innerText = hDamage.damage;
	getById("r_x").innerText = xDamage.damage;
	getById("r_d").innerText = dDamage.damage;
	getById("r_w").innerText = wDamage;
	getById("r_n").innerText = nDamage;
	getById("r_c").innerText = cDamage;
	// 计算总伤害
	var sum = bDamage.damage + hDamage.damage + xDamage.damage + dDamage.damage + wDamage + nDamage + cDamage;
	// 显示总伤害
	getById("r_t").innerText = sum;
	// 显示总伤害（暴击）
	getById("r_t2").innerText = sum * 2;
	// 基础伤害（属性+外功+内功伤害，不含穿刺伤害）
	var baseDamage = bDamage.damage + hDamage.damage + xDamage.damage + dDamage.damage + wDamage + nDamage;
	// 客户端显示伤害
	getById("r_show").innerText = "-" + baseDamage;
	getById("r_show_c").innerText = "-" + cDamage;
	// 客户端显示伤害（暴击）
	getById("r_show2").innerText = "-" + baseDamage * 2;
	getById("r_show2_c").innerText = "-" + cDamage * 2;
}
// 初始化
window.onload = init;
function init() {
	var target = getById("target");
	for (var i = 0; i < targets.length; i++) {
		target.options.add(new Option(targets[i].name, targets[i].value));
	}
	selectTarget();
}
// 获得选中的门派
function getSelectedRole() {
	var role = getById("role");
	var index = role.options.selectedIndex;
	return role[index].value;
}
// 获取选中的目标
function getSelectedTarget() {
	var target = getById("target");
	var index = target.options.selectedIndex;
	var val = target[index].value;
	var selected;
	for (var i = 0; i < targets.length; i++) {
		if (targets[i].value == val) {
			return targets[i];
		}
	}
	return null;
}
// 显示被选中目标的属性
function selectTarget() {
	var selected = getSelectedTarget();
	if (selected != null) {
		getById("t_type").innerText = selected.type == 0 ? "怪物" : "玩家";
		getById("t_name").innerText = selected.name;
		getById("t_bk").innerText = selected.bk;
		getById("t_hk").innerText = selected.hk;
		getById("t_xk").innerText = selected.xk;
		getById("t_dk").innerText = selected.dk;
		getById("t_wf").innerText = selected.wf;
		getById("t_nf").innerText = selected.nf;
		getById("t_cf").innerText = selected.cf;
	}
}
// 显示/隐藏目标属性
function switchTargetDesc() {
	var target = getById("targetDesc");
	var display = target.style.display;
	if (display == "none") {
		target.style.display = "block";
	} else {
		target.style.display = "none";
	}
	return false;
}