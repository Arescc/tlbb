/* 基础数据 */

/* 各门派10级裸属性 */
var baseAttrData = {
	"sl" : {
		"ll" : 22,
		"lq" : 3,
		"tl" : 30,
		"dl" : 6,
		"sf" : 9,
		"xsx" : 1055
	},
	"mj" : {
		"ll" : 31,
		"lq" : 3,
		"tl" : 22,
		"dl" : 3,
		"sf" : 11,
		"xsx" : 870
	},
	"gb" : {
		"ll" : 26,
		"lq" : 4,
		"tl" : 24,
		"dl" : 3,
		"sf" : 13,
		"xsx" : 960
	},
	"wd" : {
		"ll" : 3,
		"lq" : 29,
		"tl" : 16,
		"dl" : 14,
		"sf" : 8,
		"xsx" : 710
	},
	"em" : {
		"ll" : 3,
		"lq" : 23,
		"tl" : 17,
		"dl" : 19,
		"sf" : 8,
		"xsx" : 780
	},
	"xx" : {
		"ll" : 3,
		"lq" : 26,
		"tl" : 22,
		"dl" : 14,
		"sf" : 5,
		"xsx" : 830
	},
	"tl" : {
		"ll" : 15,
		"lq" : 15,
		"tl" : 16,
		"dl" : 10,
		"sf" : 14,
		"xsx" : 860
	},
	"ts" : {
		"ll" : 28,
		"lq" : 4,
		"tl" : 20,
		"dl" : 3,
		"sf" : 15,
		"xsx" : 840
	},
	"xy" : {
		"ll" : 3,
		"lq" : 23,
		"tl" : 19,
		"dl" : 12,
		"sf" : 13,
		"xsx" : 780
	},
	"mr" : {
		"ll" : 3,
		"lq" : 26,
		"tl" : 20,
		"dl" : 6,
		"sf" : 15,
		"xsx" : 850
	},
	"tm" : {
		"ll" : 3,
		"lq" : 23,
		"tl" : 19,
		"dl" : 8,
		"sf" : 16,
		"xsx" : 760
	},
	"gg" : {
		"ll" : 3,
		"lq" : 20,
		"tl" : 25,
		"dl" : 7,
		"sf" : 16,
		"xsx" : 766
	}
};

/* 每升一级时自动增加的属性 */
var lvlAttrData = {
	"sl" : {
		"ll" : 1.6,
		"lq" : 0.2,
		"tl" : 2.2,
		"dl" : 0.4,
		"sf" : 0.6,
		"xsx" : 75
	},
	"mj" : {
		"ll" : 2.2,
		"lq" : 0.2,
		"tl" : 1.6,
		"dl" : 0.2,
		"sf" : 0.8,
		"xsx" : 62
	},
	"gb" : {
		"ll" : 1.8,
		"lq" : 0.2,
		"tl" : 1.8,
		"dl" : 0.2,
		"sf" : 1,
		"xsx" : 68
	},
	"wd" : {
		"ll" : 0.2,
		"lq" : 2,
		"tl" : 1.2,
		"dl" : 1,
		"sf" : 0.6,
		"xsx" : 50
	},
	"em" : {
		"ll" : 0.2,
		"lq" : 1.6,
		"tl" : 1.2,
		"dl" : 1.4,
		"sf" : 0.6,
		"xsx" : 55
	},
	"xx" : {
		"ll" : 0.2,
		"lq" : 1.8,
		"tl" : 1.6,
		"dl" : 1,
		"sf" : 0.4,
		"xsx" : 58
	},
	"tl" : {
		"ll" : 1,
		"lq" : 1,
		"tl" : 1.2,
		"dl" : 0.8,
		"sf" : 1,
		"xsx" : 60
	},
	"ts" : {
		"ll" : 2,
		"lq" : 0.2,
		"tl" : 1.4,
		"dl" : 0.2,
		"sf" : 1.2,
		"xsx" : 60
	},
	"xy" : {
		"ll" : 0.2,
		"lq" : 1.6,
		"tl" : 1.4,
		"dl" : 0.8,
		"sf" : 1,
		"xsx" : 55
	},
	"mr" : {
		"ll" : 0.2,
		"lq" : 1.8,
		"tl" : 1.4,
		"dl" : 0.4,
		"sf" : 1.2,
		"xsx" : 60
	},
	"tm" : {
		"ll" : 0.2,
		"lq" : 1.6,
		"tl" : 1.4,
		"dl" : 0.6,
		"sf" : 1.2,
		"xsx" : 54
	},
	"gg" : {
		"ll" : 0.2,
		"lq" : 1.35,
		"tl" : 1.85,
		"dl" : 0.4,
		"sf" : 1.2,
		"xsx" : 54
	}
};

/* 门派属性成长 */
var ratioPropData = {
	"sl" : {
		"xsx" : 70,
		"qsx" : 25,
		"wg" : 7.61,
		"ng" : 3.8,
		"wf" : 8.25,
		"nf" : 5.71,
		"mz" : 6.5,
		"sb" : 2,
		"hx" : 0.04
	},
	"mj" : {
		"xsx" : 56,
		"qsx" : 20,
		"wg" : 8.25,
		"ng" : 4.44,
		"wf" : 5.71,
		"nf" : 4.44,
		"mz" : 7.5,
		"sb" : 3,
		"hx" : 0.06
	},
	"gb" : {
		"xsx" : 60,
		"qsx" : 20,
		"wg" : 7.64,
		"ng" : 3.8,
		"wf" : 7.61,
		"nf" : 5.07,
		"mz" : 8,
		"sb" : 4,
		"hx" : 0.06
	},
	"wd" : {
		"xsx" : 44,
		"qsx" : 32,
		"wg" : 4.44,
		"ng" : 8.88,
		"wf" : 4.44,
		"nf" : 7.61,
		"mz" : 7,
		"sb" : 2.5,
		"hx" : 0.05
	},
	"em" : {
		"xsx" : 50,
		"qsx" : 40,
		"wg" : 3.8,
		"ng" : 7.61,
		"wf" : 5.07,
		"nf" : 8.25,
		"mz" : 7.5,
		"sb" : 2,
		"hx" : 0.05
	},
	"xx" : {
		"xsx" : 52,
		"qsx" : 36,
		"wg" : 4.44,
		"ng" : 7.61,
		"wf" : 4.44,
		"nf" : 8.88,
		"mz" : 6,
		"sb" : 3,
		"hx" : 0.06
	},
	"tl" : {
		"xsx" : 58,
		"qsx" : 27,
		"wg" : 6.34,
		"ng" : 6.34,
		"wf" : 6.34,
		"nf" : 6.34,
		"mz" : 9,
		"sb" : 3,
		"hx" : 0.03
	},
	"ts" : {
		"xsx" : 55,
		"qsx" : 25,
		"wg" : 7.61,
		"ng" : 8.88,
		"wf" : 6.98,
		"nf" : 4.44,
		"mz" : 8,
		"sb" : 3.34,
		"hx" : 0.07
	},
	"xy" : {
		"xsx" : 50,
		"qsx" : 36,
		"wg" : 4.44,
		"ng" : 7.61,
		"wf" : 5.07,
		"nf" : 8.25,
		"mz" : 7.5,
		"sb" : 4,
		"hx" : 0.05
	},
	"mr" : {
		"xsx" : 60,
		"qsx" : 25,
		"wg" : 4.44,
		"ng" : 8.88,
		"wf" : 6.98,
		"nf" : 5.07,
		"mz" : 7.5,
		"sb" : 4,
		"hx" : 0.05
	},
	"tm" : {
		"xsx" : 55,
		"qsx" : 36,
		"wg" : 4.44,
		"ng" : 8.25,
		"wf" : 4.44,
		"nf" : 8.88,
		"mz" : 8.5,
		"sb" : 3,
		"hx" : 0.06
	},
	"gg" : {
		"xsx" : 58,
		"qsx" : 35,
		"wg" : 4.44,
		"ng" : 8.25,
		"wf" : 4.44,
		"nf" : 8.9,
		"mz" : 7.8,
		"sb" : 3.5,
		"hx" : 0.05
	},
};
